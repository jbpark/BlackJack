import java.util.*;

class Card{
	
	public enum Shape{HEART, DIAMOND, CLUB, SPADE}
	public enum Value{
		ACE (11), TWO (2), THREE (3), FOUR (4), FIVE (5), SIX (6), 
		SEVEN (7),EIGHT (8), NINE (9), TEN (10), JACK (10), 
		QUEEN (10), KING (10);
					
		private final int val;
					
		Value(int val){
			this.val = val;
		}
		
		public int get_val(){
			return this.val;
		}
	}
	
	private Shape shape;
	private Value value;
	
	public void set_shape(Shape shape){
		this.shape = shape;
	}
	
	public Shape get_shape(){
		return this.shape;
	}
	
	public void set_value(Value value){
		this.value = value;
	}
	
	public Value get_value(){
		return this.value;
	}
	
	public void display_card(){
		System.out.println(this.value + " of " + this.shape);
	}
}

class Deck{
	
	private List<Card> deck = new ArrayList<Card>();
	final int num_cards = 52;
	
	public void create_Deck(){		
		for(int i=0; i<4;i++){
			for(int j=0; j<13;j++){
				Card card = new Card();
				card.set_value(Card.Value.values()[j]);
				card.set_shape(Card.Shape.values()[i]);
				deck.add(card);
			}
		}
	}
	
	public void display_Deck(){
		for(int i=0; i<num_cards; i++){
			System.out.println(deck.get(i).get_value() + " of " 
										+ deck.get(i).get_shape());
		}
	}
	
	public void shuffle_Deck(){
		Random r = new Random();
		for(int i=0; i<num_cards; i++){
			int rPos = r.nextInt(deck.size());
			Card sPos = deck.get(i);
			deck.set(i, deck.get(rPos));
			deck.set(rPos, sPos);
		}
	}
	
	public Card draw_card(){
		return deck.remove(0);
	}
}

class Play_BJack extends Deck{
	
	private List<Card> player_hand = new ArrayList<Card>();
	private List<Card> dealer_hand = new ArrayList<Card>();
	private boolean game_lost;
	private String hit;
	
	Scanner in = new Scanner(System.in);
	
	
	public void play(){
		init();
		do{
			System.out.println("Do you want to hit? (y/n)");
			hit = in.next();
			if(hit.equals("y")){
				hit(player_hand);
				System.out.println("The following is your hand:");
				display_hand(player_hand);
				game_lost = sum_hand(player_hand) < 22 ? false:true;
			} else if(hit.equals("n")){
				break;
			} else{
				System.out.print("Error! ");
			}
				
		}while(!game_lost);
		
		
		if(game_lost){
			System.out.println("You have lost the game!");
		}else{
			while((sum_hand(dealer_hand) < 16 && sum_hand(dealer_hand) < sum_hand(player_hand))){
				System.out.println("Dealer has hit!");
				hit(dealer_hand);
			}
			
			System.out.println("The following is the dealer's hand:");
			display_hand(dealer_hand);
			if(sum_hand(dealer_hand) > 21){
				System.out.println("You have won the game!");
			} else if(sum_hand(dealer_hand) > sum_hand(player_hand)){
				System.out.println("You have lost the game!");
			} else if(sum_hand(dealer_hand) < sum_hand(player_hand)){
				System.out.println("You have won the game!");
			} else{
				System.out.println("The game is a draw!");
			}
		}
		
	}
	
	public void init(){
		player_hand.add(draw_card());
		dealer_hand.add(draw_card());
		player_hand.add(draw_card());
		dealer_hand.add(draw_card());
		System.out.println("The following is the first card of dealer's hand:");
		dealer_hand.get(0).display_card();
		System.out.println("The following is your hand:");
		display_hand(player_hand);
	}
	
	public void hit(List<Card> hand){
		hand.add(draw_card());

	}

	
	public void display_hand(List<Card> hand){
		for (Card card:hand){
			card.display_card();
		}
	}
	
	public int sum_hand(List<Card> hand){
		int sum = 0;
		int ace_counter = 0;;
		for (Card card:hand){
			if(card.get_value() == Card.Value.ACE){
				ace_counter++;
			}
			sum += card.get_value().get_val();
		}
		while (sum>21){
			if(ace_counter>0){
				sum -= 10;
				ace_counter--;
			}else{
				break;
			}
		}
		return sum;
	}
}

public class BJack{
	public static void main(String[] args){
		Play_BJack game = new Play_BJack();
		game.create_Deck();
		game.shuffle_Deck();
		game.play();
	}
}